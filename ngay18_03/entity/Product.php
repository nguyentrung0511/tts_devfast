<?php
require_once('./../abstract/BaseRow.php');

class Product extends BaseRow
{
    private $id;
    private $name;
    public $categoryId;

    public function __construct( $id, $name, $categoryId)
    {
        $this->id = $id;
        $this->name = $name;
        $this->categoryId = $categoryId;
    }

}


