<?php
require_once('./Database.php');
class AccessoryDAO
{
    public function __construct()
    {
        $database = new Database();
    }
    public function insert( $row)
    {
        $database->insertTable('accessotionTable', $row);
    }
    public function update( $row)
    {
        $database->updateTable('accessotionTable', $row);
    }
    public function delete( $row)
    {
        $database->deleteTable('accessotionTable', $row);
    }
    public function findAll()
    {
        return $database->selectTable('accessotionTable');
    }
    public function findById( $id)
    {
        $database->selectTable('accessotionTable', $id);
    }
}
