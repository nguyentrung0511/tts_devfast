<?php
require('Database.php');

class ProductDao
{
    public function __construct()
    {
        $database = new Database();
    }
    public function insert( $row)
    {
        $database->insertTable('productTable', $row);
    }
    public function update( $row)
    {
        $database->updateTable('productTable', $row);
    }
    public function delete( $row)
    {
        $database->deleteTable('productTable', $row);
    }
    public function findAll()
    {
        return $database->selectTable('productTable');
    }
    public function findById( $id)
    {
        $database->selectTable('productTable', $id);
    }
}
