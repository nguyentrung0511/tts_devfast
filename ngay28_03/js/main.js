// bài 9:
class Main{
    constructor(id, name, categoryId, saleDate, qulyity, isdelete) {
        this.id = id;
        this.name = name;
        this.categoryId = categoryId;
        this.saleDate = saleDate;
        this.qulyity = qulyity;
        this.isdelete = isdelete;
    }
}
// bài 10
function listMain(){
    const pushlistproduct = [];
    let main1 = new Main(1, 'Bánh kem ', 1, new Date(2022, 3, 28, 10, 40, 30), 0, true);
    pushlistproduct.push(main1);
    let main2 = new Main(2, 'Bánh sukem ', 1, new Date(2022, 3, 28, 10, 40, 30), 2, true);
    pushlistproduct.push(main2);
    let main3 = new Main(3, 'Kẹo kéo ', 1, new Date(2022, 3, 28, 10, 40, 30), 0, true);
    pushlistproduct.push(main3);
    let main4 = new Main(4, 'Coca Cola ', 3, new Date(2022, 3, 28, 10, 40, 30), 1, false);
    pushlistproduct.push(main4);
    let main5 = new Main(5, 'So co la ', 1, new Date(2022, 3, 29, 10, 40, 30), 2, false);
    pushlistproduct.push(main5);
    let main6 = new Main(6, 'Mỳ tôm ', 2, new Date(2022, 3, 28, 10, 40, 30), 0, true);
    pushlistproduct.push(main6);
    let main7 = new Main(7, 'Kem ', 1, new Date(2022, 3, 29, 10, 40, 30), 3, false);
    pushlistproduct.push(main7);
    let main8 = new Main(8, 'Bò cười ', 3, new Date(2022, 3, 28, 10, 40, 30), 1, true);
    pushlistproduct.push(main8);
    let main9 = new Main(9, 'Pate ', 1, new Date(2022, 3, 28, 10, 40, 30), 0, true);
    pushlistproduct.push(main9);
    let main10 = new Main(10, 'Đô ăn Tàu ', 1, new Date(2022, 3, 28, 10, 40, 30), 0, true);
    pushlistproduct.push(main10);
    return pushlistproduct;
}
const listProduct = listMain();
console.log(listProduct);
// bài 11
// cách làm theo for
function fiterProductByIdFor(listProduct, idProduct){
    for (let product of listProduct){
        if(product.id == idProduct){
            return product
        }
    }
    return false
}
const checkidfor = fiterProductByIdFor(listProduct,1);
if (checkidfor != false){
    console.log(checkidfor);
}else {
    console.log('Không có id đó');
}
//cách làm theo ES6
function fiterProductById(listProduct, idProduct){
     const productname = listProduct.find(product => product.id === idProduct);
        return productname;
}
console.log(fiterProductById(listProduct,1));

//bài 12
// cách dùng for
function fiterProductByQulityFor(listProduct){
    const pushlistproduct = [];
    for (let product of listProduct){
        if(product.qulyity > 0 && product.isdelete === false){
            pushlistproduct.push(product);
        }
    }
    if(!pushlistproduct.length){
        return false
    }
    return pushlistproduct
}
const checklistproductfor = fiterProductByQulityFor(listProduct);
if (checklistproductfor !== false){
    console.log(checklistproductfor);
}else {
    console.log('Không có giá trị nào hợp lệ');
}
// cách dùng ES6
function fiterProductByQulity(listProduct){
    const filterproduct = listProduct.filter(product => product.qulyity > 0 && product.isdelete === false);
    return filterproduct;
}

console.log(fiterProductByQulity(listProduct));
// bài 13
// cách dùng for
function fiterProductBySaleDateFor(listProduct){
    var curDate = new Date()
    const pushlistproduct = [];
    for (let product of listProduct){
        if(product.saleDate - curDate.getDate() > 0 && product.isdelete === false){
            pushlistproduct.push(product);
        }
    }
    if(!pushlistproduct.length){
        return false
    }
    return pushlistproduct
}
const checksaledatefor = fiterProductBySaleDateFor(listProduct);
if (checksaledatefor !== false){
    console.log(checksaledatefor);
}else {
    console.log('Không có ngày khuyến mại nào');
}
// cách dùng ES6
function fiterProductBySaleDate(listProduct){
    var Datenow = new Date();
    const filterproduct = listProduct.filter(product => product.saleDate - Datenow.getDate() > 0 && product.isdelete === false);
    return filterproduct;
}

console.log(fiterProductBySaleDate(listProduct));
//bài 14
// cách dùng reduce
function totalProductReduce(listProduct){
    const listfilterproduct = listProduct.filter(product => product.qulyity > 0 && product.isdelete === false);
    const listreduceproduct = listfilterproduct.reduce((total, listfilterproduct) => total += listfilterproduct.qulyity, 0);
    return listreduceproduct;
}

console.log(totalProductReduce(listProduct));
// cách không dùng reduce
function totalProductNoReduce(listProduct){
    let total = 0;
    const listfilterproduct = listProduct.filter(product => product.qulyity > 0 && product.isdelete === false);
    for (producttotal of listfilterproduct){
        total += producttotal.qulyity;
    }
    return total;
}
console.log(totalProductNoReduce(listProduct));
//bài 15
// cách dùng for
function isHaveProductInCategoryFor(listProduct, categoryId){
    for (let product of listProduct){
        if(product.categoryId === categoryId){
            return true;
            break;
        }
    }
    return false
}
const checkcategoryIdfor = isHaveProductInCategoryFor(listProduct, 2);
if (checkcategoryIdfor === true){
    console.log('Có giá trị hợp lệ');
}else {
    console.log('Không có giá trị nào hợp lệ');
}
// cách dùng ES6
function isHaveProductInCategory(listProduct, categoryId){
    const categoryid = listProduct.find(product => product.categoryId === categoryId);
    if (categoryid){
        return true
    }
    return false
}
const checkcategoryId = isHaveProductInCategory(listProduct, 2);
if (checkcategoryId === true){
    console.log('Có giá trị hợp lệ');
}else {
    console.log('Không có giá trị nào hợp lệ');
}
//bài 16
//cách dùng for
function fiterProductBySaleDateQulityFor(listProduct){
    var curDate = new Date()
    const pushlistproduct = [];
    for (let product of listProduct){
        if(product.saleDate - curDate.getDate() > 0 && product.isdelete === false && product.qulyity > 0){
            pushlistproduct.push(product.id, product.name);
        }
    }
    if(!pushlistproduct.length){
        return false
    }
    return pushlistproduct
}
const checksaledatequlityfor = fiterProductBySaleDateQulityFor(listProduct);
if(checksaledatequlityfor !== false){
    console.log(checksaledatequlityfor);
}else {
    console.log('Không có giá trị nào hợp lệ');
}
// cách dùng ES6
function fiterProductBySaleDateQulity(listProduct){
    var curDate = new Date()
    const filterproductsaledate = listProduct.filter(product => product.saleDate - curDate.getDate() > 0 && product.isdelete === false && product.qulyity > 0);
    const listreduceproduct = filterproductsaledate.map((item) => {
        return {id: item.id, name:item.name}
    });
    return listreduceproduct;
}
console.log(fiterProductBySaleDateQulity(listProduct));