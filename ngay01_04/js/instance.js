var obj = {
    foo: 'bar'
  }
  
  Object.freeze(obj)
  
  new Vue({
    el: '#app',
    data: obj
  })
  new Vue({
    data: {
      a: 1
    },
    created: function () {
      // `this` trỏ đến đối tượng Vue hiện hành
      console.log('giá trị của a là ' + this.a)
    }
  })
  // => "giá trị của a là 1"