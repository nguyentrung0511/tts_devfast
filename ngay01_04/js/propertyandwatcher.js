var vm = new Vue({
    el: '#example',
    data: {
      message: 'người đông bến đợi thuyền xuôi ngược'
    },
    computed: {
      // một computed getter
      reversedMessage: function () {
        // `this` trỏ tới đối tượng vm
        return this.message.split(' ').reverse().join(' ')
      }
    }
  })
  console.log(vm.reversedMessage) // => 'ngược xuôi thuyền đợi bến đông người'
vm.message = 'xa ngân tiếng hát đàn trầm bổng'
console.log(vm.reversedMessage) // => 'bổng trầm đàn hát tiếng ngân
var vm = new Vue({
  el: '#demo',
  data: {
    firstName: 'Evan',
    lastName: 'You'
  },
  computed: {
    fullName: function () {
      return this.firstName + ' ' + this.lastName
    }
  }
})