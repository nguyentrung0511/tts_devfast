<?php
require_once('./../dao/Database.php'); 
require_once('./../entity/Product.php');
require_once('./../entity/Category.php');
require_once('./../entity/Accessotion.php');

class DatabaseDemo extends Database
{
    /**
     * Insert row to Table
     * @param  $name
     * @param  $row
     * @return void
     */
    public function insertTableTest($name, $row)
    {
        $this->insertTable($name, $row);
    }

    /**
     * Select all row from Table
     * @param  $name
     * @return mixed
     */
    public function selectTableTest($name, $whereId=null)
    {
        return $this->selectTable($name, $whereId);
    }

    /**
     * Update row from Table by ID
     * @param  $name
     * @param  $row
     * @return void
     */
    public function updateTableTest($name, $row)
    {
        $this->updateTable($name, $row);
    }

    /**
     * Delete row from Table by ID
     * @param  $name
     * @param  $row
     * @return void
     */
    public function deleteTableTest($name, $row)
    {
        $this->deleteTable($name, $row);
    }

    /**
     * Delete all row from Table
     * @param  $name
     * @return void
     */
    public function truncateTableTest( $name)
    {
        $this->truncateTable($name);
    }

    /**
     * Update row from Table by ID
     * @param  $id
     * @param  $row
     * @return void
     */
    public function updateTableByIdTest($id, $row)
    {
        $this->updateTableById($id, $row);
    }

    /**
     * Init row to Table
     * @param  $name
     * @param  $row
     * @return void
     */
    public function initDatabase()
    {
        
        for($i = 1; $i<=10 ; $i++)
        {
            $product = new Product($i, 'Bánh ngọt '.$i, 2);
            $this->insertTable('productTable', $product);

            $category = new Category($i, 'Loại bánh '.$i);
            $this->insertTable('categoryTable', $category);

            $accessotion = new Accessotion($i, 'Accessotion '.$i);
            $this->insertTable('accessotionTable', $accessotion);
        }
    }

}

$database = DatabaseDemo::getInstants('DatabaseDemo');

$database->initDatabase();
echo '<pre>';
print_r($database->selectTableTest('productTable'));

echo strtolower(get_class($database)).'Table';

