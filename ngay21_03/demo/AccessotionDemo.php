<?php
require_once('./../entity/Accessotion.php');

class AccessotionDemo extends Accessotion
{
    public function __construct()
    {
       
    }

    /**
     * Create instants Accessotion
     * @param  $id
     * @param  $name
     * return mixed
     */
    public function createAccessotionTest($id, $name)
    {
        $accessotion =  new Accessotion($id, $name);
        return $accessotion;
    }

    /**
     * Print Accessotion
     * @param Accessotion $accessotion
     * return mixed
     */
    public function printAccessotion(Accessotion $accessotion)
    {
        echo 'Accessotion ID: '.$accessotion->getId().'<br/>';
        return $accessotion;
    }
}
