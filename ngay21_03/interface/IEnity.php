<?php

interface IEnity
{
    /**
     * get ID by instant
     * @return string
     */
    public function getId();

    /**
     * get Name by instant
     * @return string
     */
    public function getName();
}