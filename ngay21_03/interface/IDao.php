<?php

interface IDao
{
    /**
     * Insert row to categoryTable
     * @param  $row
     * @return void
     */
    public function insert($row);

    /**
     * Update row categoryTable by ID
     * @param  $row
     * @return void
     */
    public function update($row);

    /**
     * delete row from categoryTable by ID
     * @param  $row
     * @return void
     */
    public function delete($row);

    /**
     * Select All row categoryTable
     * @return mixed
     */
    public function findAll($name);

    /**
     * Select row by ID categoryTable
     * @return mixed
     */
    public function findById($name, $id);

}