<?php
class Database{
    public $productTable = array();
    public $catagoryTable = array();
    public $accessoryTable = array();
    protected static  $instants = null;
    public function isertTable($name, $row){
            $this->$name[]= $row;
            return 1;
    }
    public function selectTable($name, $where){
        foreach ($this->$name as $value){
            if($value->getName() == $where){
                return $value;
            }
        }
    }
    public function updateTable($name,$row){
        foreach ($this->$name as $key => $value){
            if($this->$name[$key]->getId() == $row->getId()){
                $this->$name[$key] = $row;
            }
        }
        return 1;
    }
    public function deleteTable($name,$row){
        foreach ($this->$name as $key => $value){
            if($this->$name[$key]->getId() == $row->getId()){
                unset($this->{$name}[$key]);
            }
        }
        return 1;
    }
    public function truncateTable($name)
    {
        $this->{$name} = null;
    }
    public function updateTableById( $id, $row)
    {

            switch(get_class($row))
            {
                case 'Product' :
                    foreach($this->productTable as $key=>$product)
                    {
                        if($product->getId() == $id)
                        {
                            $this->productTable[$key] = $row;
                        }
                    }
                    break;
                case 'Category' :
                    foreach($this->categoryTable as $key=>$category)
                    {
                        if($category->getId() == $id)
                        {
                            $this->categoryTable[$key] = $row;
                        }
                    }
                    break;
                case 'Accessotion' :
                    foreach($this->accessotionTable as $key=>$accessotion)
                    {
                        if($accessotion->getId() == $id)
                        {
                            $this->accessotionTable[$key] = $row;
                        }
                    }
                    break;
            }
            return 1;
    }
}