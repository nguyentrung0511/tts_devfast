<?php
function mapProductByCategory($listProduct, $listCategory) {
    $countCategory= count($listCategory);
    $push = array();
    for ($i=0; $i< $countCategory;$i++){
        $push[]=pushArrayProduct($i,$listProduct,$listCategory);
    }
    return $push;
}
function pushArrayProduct($i,$listProduct,$listCategory){
    $arrayproduct = array();
    $countProduct = count($listProduct);
    for ($j=0; $j< $countProduct-1; $j++){
        if($listCategory[$i]['id'] == $listProduct[$j]['categroyID']){
            $arrayproduct[]= $listProduct[$j];
            $arrayproduct[]=$listCategory[$i]['name'];

        }
    }
    return $arrayproduct;
}

$listProduct = [
    ['name'=>'CPU', 'price'=>750, 'quality'=>10, 'categroyID'=>1],
    ['name'=>'RAM', 'price'=>50, 'quality'=>2, 'categroyID'=>2],
    ['name'=>'HDD', 'price'=>70, 'quality'=>1, 'categroyID'=>2],
    ['name'=>'Main', 'price'=>400, 'quality'=>3, 'categroyID'=>1],
    ['name'=>'Keyboard', 'price'=>30, 'quality'=>8, 'categroyID'=>1],
    ['name'=>'Mouse', 'price'=>25, 'quality'=>50, 'categroyID'=>4],
    ['name'=>'VGA', 'price'=>60, 'quality'=>35, 'categroyID'=>4],
    ['name'=>'Monitor', 'price'=>120, 'quality'=>28, 'categroyID'=>2],
    ['name'=>'Case', 'price'=>120, 'quality'=>28, 'categroyID'=>5]
];
$listCategory =[
    ['id'=>1, 'name'=>'Comuter'],
    ['id'=>2, 'name'=>'Memory'],
    ['id'=>3, 'name'=>'Card'],
    ['id'=>4, 'name'=>'Acsesory']
];
print_r(mapProductByCategory($listProduct,$listCategory));
?>