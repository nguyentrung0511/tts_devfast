<?php
function findProduct($listProduct, $nameProduct) {
    $array = array();
    foreach($listProduct as $value){
        if($value['name']==$nameProduct) {
            $array[] = $value;
        }
    }
    if(empty($array)){
        return false;
    }
    return $array;
}
$listProduct = [
    ['name'=>'CPU', 'price'=>750, 'quality'=>10, 'categroyID'=>1],
    ['name'=>'RAM', 'price'=>50, 'quality'=>2, 'categroyID'=>2],
    ['name'=>'HDD', 'price'=>70, 'quality'=>1, 'categroyID'=>2],
    ['name'=>'Main', 'price'=>400, 'quality'=>3, 'categroyID'=>1],
    ['name'=>'Keyboard', 'price'=>30, 'quality'=>8, 'categroyID'=>1],
    ['name'=>'Mouse', 'price'=>25, 'quality'=>50, 'categroyID'=>4],
    ['name'=>'VGA', 'price'=>60, 'quality'=>35, 'categroyID'=>4],
    ['name'=>'Monitor', 'price'=>120, 'quality'=>28, 'categroyID'=>2],
    ['name'=>'Case', 'price'=>120, 'quality'=>28, 'categroyID'=>5]
];
$nameProduct = 'CPU';
$check = findProduct($listProduct,$nameProduct);
if ($check  != false){
    print_r($check);
}else{
    echo 'Tên sản phẩm nhập vào không đúng';
}
?>