Vue.component('button-counter', {
    data() {
        return {
            count: 0
        }
    },
    template: '<button v-on:click="count++">Clicked : {{ count }} .</button>'
});
Vue.component('custom-input', {
    props: ['value'],
    template: `
      <input
        v-bind:value="value"
        v-on:input="$emit('input', $event.target.value)"
      >
    `
})
Vue.component('product-item', {
    props: ['product'],
    template: `
    <div class="product">
        <h3 class="btn-delete">{{ product.name }}</h3>
        <button @click="deleteProduct">delete</button>
    </div>
    `,
    methods: {
        deleteProduct() {
            var data = {
                id: this.product.id
            }
            this.$emit('handle-delete', data)
        }
    },
})
Vue.component('alert-box', {
    template: `
      <div class="alert-box">
        <strong>Error!</strong>
        <slot></slot>
      </div>
    `
})
var app = new Vue({
    el: "#app",
    data: {
        products: [
            { id: 1, name: 'Iphone' },
            { id: 2, name: 'Samsung' },
            { id: 3, name: 'Xiaomi' }
        ],
        postFontSize: 2,
        searchText: ''
    },
    methods: {
        handleDelete(data) {
            var indexDelete;
            this.products.forEach((prd, index) => {
                if (prd.id == data.id) {
                    indexDelete = index;
                }
            });
            console.log(indexDelete);
            this.products.splice(indexDelete, 1);
        }
    }
});