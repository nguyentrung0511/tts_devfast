<?php
/*Cách 1: Không theo đệ quy*/
function calMonth($money, $rate) {
    $i=0;
    do{
        $mon= $money + ($money*$rate)/100;
        $money = $mon;
        $i++;
    }while ($mon < $money * 2 );
    return $i;
}
echo calMonth(1000,5);
/*Cách 2: Theo đệ quy*/
function calMonth1($money, $rate, $n= 1) {
    if ($money + $money * ($rate / 100)*$n >= $money * 2) {
        return $n;
    }
    return calMonth1($money, $rate);
}
echo calMonth1(100,5);